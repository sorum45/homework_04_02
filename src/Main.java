import java.util.Arrays;

public class Main {
    // Функция по сложению чисел в массиве в заданном диапазоне
    public static int calcSumArrayRange (int [] a, int from, int to) {
        int sum = 0;
        if (from < to){
            for (int i = from; i <=to; i++){
                sum += a[i];
            }
        }
        else if ( from > to){
            sum = -1;
        }
        return sum;
    }

    // функция по извелчению числа из массива, НЕ ИДЕАЛЬНА!!!!
    public static int numberArray ( int [] b){
        int toInt = 0;
        for(int j = 0; j <=b.length -1; j++){
            toInt = toInt * 10 + b[j];
            System.out.println(toInt);
        }
        return toInt;
    }
    // процедура по выводу всех четных чисел в массиве
    public static void printEvenNumbersArray ( int [] a) {
        int[] counts = new int[10];
        for (int i = 0; i <=a.length -1; i++){
            if (a[i] % 2 == 0 ){
                counts[i] = a[i];
            }
        }
        System.out.println(Arrays.toString(counts));
    }
    public static void main(String[] args) {

        int [] x = {24, 34, 78, 23, 12, 82, 37, 65, 54, 72};
        int sum1 = calcSumArrayRange(x, 3, 8);
        System.out.println(sum1);

        printEvenNumbersArray(x);

        int [] y = {9, 8, 7, 2, 3};
        int toInt = numberArray(x);
        System.out.println(toInt);
    }
}